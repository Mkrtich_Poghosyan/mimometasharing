package com.android.mimo.network.di

import android.content.Context
import com.android.mimo.network.connectivity.NetworkConnectivityObserver
import com.android.mimo.network.observer.ConnectivityObserver
import org.koin.core.annotation.ComponentScan
import org.koin.core.annotation.Module
import org.koin.core.annotation.Single


@Module
@ComponentScan("com.android.mimo.network")
class NetworkModule {

    @Single
    fun providesConnectivityChange(context : Context) : ConnectivityObserver {
        return NetworkConnectivityObserver(context)
    }

}