package com.android.mimo.entities.user.response.settings

data class Settings(
    val locale: String?,
    val mode: String?,
    val sendPush: Boolean = true
)
