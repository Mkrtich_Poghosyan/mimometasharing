package com.android.mimo.entities.user.response.avatar

data class Avatar(
    val id: String,
    val node: String?
)