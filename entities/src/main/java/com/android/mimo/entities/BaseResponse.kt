package com.android.mimo.entities

data class BaseResponse<T>(
    val timestamp: String,
    val statusCode: Int,
    val status: String,
    val message: String,
    var content: T?
)