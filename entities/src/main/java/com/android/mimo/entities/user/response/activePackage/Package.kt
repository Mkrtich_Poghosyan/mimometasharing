package com.android.mimo.entities.user.response.activePackage

data class Package(
    val end: Long?,
    val id: String?,
    val name: String?,
    val start: Long?
)
