package com.android.mimo.entities.user.response.gender

enum class Gender(sex : String?) {

    MALE("MALE"),
    FEMALE("FEMALE");

    companion object {
        fun getSex(sex: String?): Gender? {
            return when (sex) {

                "MALE" -> MALE
                "FEMALE" -> FEMALE
                else -> null
            }
        }

        fun getSex(position: Int) : Gender?{
            return when(position){
                0 -> MALE
                1 -> FEMALE
                else -> null
            }
        }
    }
}
