package com.android.mimo.entities.user.response.tariff

data class Tariff(
    val end: Long?,
    val id: String?,
    val name: String?,
    val start: Long?
)