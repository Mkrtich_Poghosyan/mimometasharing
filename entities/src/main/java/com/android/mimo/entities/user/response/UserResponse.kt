package com.android.mimo.entities.user.response

import com.android.mimo.entities.user.response.avatar.Avatar
import com.android.mimo.entities.user.response.gender.Gender
import com.android.mimo.entities.user.response.settings.Settings
import com.android.mimo.entities.user.response.tariff.Tariff

data class UserResponse(
    val avatar: Avatar?,
    val bio: String?,
    val birthday: String?,
    val distance: Int?,
    var email: String?,
    val emailVerified: Boolean,
    val gender: Gender?,
    val lastActionDate: Long?,
    val minutes: Int?,
    val name: String?,
    val `package`: Package?,
    val settings: Settings,
    val status: String?,
    val surname: String?,
    val tariff: Tariff?
)
