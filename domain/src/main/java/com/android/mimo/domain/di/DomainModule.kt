package com.android.mimo.domain.di

import org.koin.core.annotation.ComponentScan
import org.koin.core.annotation.Module

@Module
@ComponentScan("com.android.mimo.domain")
class DomainModule {
}