package com.android.mimo.domain.baseusecase

import com.android.mimo.core.ActionResult
import com.example.domain.dispatcher.CoroutineDispatcherProvider
import kotlinx.coroutines.flow.FlowCollector
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject


abstract class DataStateUseCase<in Params, out ReturnType> : KoinComponent {

    protected abstract suspend fun FlowCollector<ActionResult<ReturnType>>.execute(params: Params)

    private val dispatcher: CoroutineDispatcherProvider by inject()

    suspend fun invoke(params: Params) = flow {
        execute(params)
    }.flowOn(dispatcher.io)

}
