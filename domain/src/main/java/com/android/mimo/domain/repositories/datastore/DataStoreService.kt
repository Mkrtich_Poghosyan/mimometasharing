package com.android.mimo.domain.repositories.datastore

import kotlinx.coroutines.flow.Flow

interface DataStoreService {

    suspend fun setString(key : String, value : String?)
    fun getString(key : String) : Flow<String?>

    suspend fun setInt(key : String, value : Int)
    fun getInt(key : String, defaulterValue : Int) : Flow<Int>

    suspend fun setBoolean(key : String, value : Boolean)
    fun getBoolean(key : String, defaulterValue : Boolean) : Flow<Boolean>

    suspend fun setFloat(key : String, value : Float)
    fun getFloat(key : String, defaulterValue : Float) : Flow<Float>

    suspend fun setDouble(key : String, value : Double)
    fun getDouble(key : String, defaulterValue : Double) : Flow<Double>

    suspend fun setLong(key : String, value : Long)
    fun getLong(key : String, defaulterValue : Long) : Flow<Long>


}