package com.android.mimo.domain.baseusecase

import androidx.paging.PagingData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn

abstract class FlowPagingUseCase<in Params, ReturnType : Any>  {

    protected abstract suspend fun execute(params: Params): Flow<PagingData<ReturnType>>

    suspend operator fun invoke(params: Params): Flow<PagingData<ReturnType>> = execute(params)
        .flowOn(Dispatchers.IO)
}