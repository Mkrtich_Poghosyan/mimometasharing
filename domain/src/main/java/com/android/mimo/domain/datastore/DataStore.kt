package com.android.mimo.domain.datastore

import com.android.mimo.domain.model.SwitchState
import kotlinx.coroutines.flow.Flow

interface DataStore {

    companion object{
       const val ACCESS_TOKEN = "token"
       const val REFRESH_TOKEN = "refresh_token"
       const val TOKEN_TYPE = "token_type"
       const val LANGUAGE = "locale"
       const val DEVICE_ID = "device_id"
       const val PHONE_CODE = "phone_code"
       const val PHONE = "phone"
       const val EMAIL = "email"
       const val FIRST_RUN = "first_run"
       const val FIRST_ON_BOARDING = "first_on_boarding"
       const val FIREBASE_TOKEN = "firebase_token"
       const val SWITCH_STATE = "stat_model"
       const val ORDER_CARD = "order_card_time"
       const val STORY_TIME= "story_time"

    }

    suspend fun setAccessToken(token: String?)
    fun getAccessToken(): Flow<String?>

    suspend fun setRefreshToken(token: String?)
    fun getRefreshToken(): Flow<String?>

    suspend fun setLanguage(language: String?)
    fun getLanguage(): Flow<String?>

    suspend fun setTokenType(tokenType: String?)
    fun getTokenType(): Flow<String?>

    suspend fun setDeviceID(deviceID: String?)
    fun getDeviceID(): Flow<String?>

    suspend fun setPhoneCode(phoneCode: String?)
    fun getPhoneCode(): Flow<String?>

    suspend fun setPhoneNumber(phoneNumber: String?)
    fun getPhoneNumber(): Flow<String?>

    suspend fun setEmail(email: String?)
    fun getEmail(): Flow<String?>

    suspend fun setFirstRun(isFirstRun: Boolean)
    fun isFirstRun(): Flow<Boolean>

    suspend fun setFirstOnBoarding(isFirstOnBoarding: Boolean)
    fun isFirstOnBoarding(): Flow<Boolean>

    suspend fun setFirebaseToken(firebaseToken: String?)
    fun getFirebaseToken(): Flow<String?>

    suspend fun setSwitchState(switchState: SwitchState)
    fun getSwitchState(): Flow<SwitchState>

    suspend fun setOrderCardTime(timeInMillis: Long)
    fun getOrderCardTime(): Flow<Long>

    suspend fun setStoryTime(storyTime: Long)
    fun getStoryTime(): Flow<Long>

    suspend fun logOut()

}