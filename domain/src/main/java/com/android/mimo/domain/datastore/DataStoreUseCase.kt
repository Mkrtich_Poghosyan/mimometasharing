package com.android.mimo.domain.datastore

import com.android.mimo.domain.model.SwitchState
import com.android.mimo.domain.repositories.datastore.DataStoreService
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import org.koin.core.annotation.Factory
import org.koin.core.annotation.Named

@Factory
@Named("DataStoreUseCase")
class DataStoreUseCase(private val storeService: DataStoreService) : DataStore {

    override suspend fun setLanguage(language: String?) {
        storeService.setString(DataStore.LANGUAGE, language)
    }

    override fun getLanguage(): Flow<String?> = storeService.getString(DataStore.LANGUAGE)

    override suspend fun setAccessToken(token: String?) {
        storeService.setString(DataStore.ACCESS_TOKEN, token)
    }

    override fun getAccessToken(): Flow<String?> = storeService.getString(DataStore.ACCESS_TOKEN)

    override suspend fun setRefreshToken(token: String?) {
        storeService.setString(DataStore.REFRESH_TOKEN, token)
    }

    override fun getRefreshToken(): Flow<String?> = storeService.getString(DataStore.REFRESH_TOKEN)

    override suspend fun setTokenType(tokenType: String?) {
        storeService.setString(DataStore.TOKEN_TYPE, tokenType)
    }

    override fun getTokenType(): Flow<String?> = storeService.getString(DataStore.TOKEN_TYPE)

    override suspend fun setDeviceID(deviceID: String?) {
        storeService.setString(DataStore.DEVICE_ID, deviceID)
    }

    override fun getDeviceID(): Flow<String?> = storeService.getString(DataStore.DEVICE_ID)

    override suspend fun setPhoneCode(phoneCode: String?) {
        storeService.setString(DataStore.PHONE_CODE, phoneCode)
    }

    override fun getPhoneCode(): Flow<String?> = storeService.getString(DataStore.PHONE_CODE)

    override suspend fun setPhoneNumber(phoneNumber: String?) {
        storeService.setString(DataStore.PHONE, phoneNumber)
    }

    override fun getPhoneNumber(): Flow<String?> = storeService.getString(DataStore.PHONE)

    override suspend fun setEmail(email: String?) {
        storeService.setString(DataStore.EMAIL, email)
    }

    override fun getEmail(): Flow<String?> = storeService.getString(DataStore.EMAIL)

    override suspend fun setFirstRun(isFirstRun: Boolean) {
        storeService.setBoolean(DataStore.FIRST_RUN, isFirstRun)
    }

    override fun isFirstRun(): Flow<Boolean> = storeService.getBoolean(DataStore.FIRST_RUN, defaulterValue = true)

    override suspend fun setFirstOnBoarding(isFirstOnBoarding: Boolean) {
        storeService.setBoolean(DataStore.FIRST_ON_BOARDING, isFirstOnBoarding)
    }

    override fun isFirstOnBoarding(): Flow<Boolean> = storeService.getBoolean(DataStore.FIRST_ON_BOARDING, defaulterValue = true)

    override suspend fun setFirebaseToken(firebaseToken: String?) {
        storeService.setString(DataStore.FIREBASE_TOKEN, firebaseToken)
    }

    override fun getFirebaseToken(): Flow<String?> = storeService.getString(DataStore.FIREBASE_TOKEN)

    override suspend fun setSwitchState(switchState: SwitchState) {
        storeService.setInt(DataStore.SWITCH_STATE, switchState.ordinal)
    }

    override fun getSwitchState(): Flow<SwitchState> = flow {
        val state = storeService.getInt(DataStore.SWITCH_STATE, 0).first()
        emit(
            SwitchState.values()[state]
        )
    }

    override suspend fun setOrderCardTime(timeInMillis: Long) {
        storeService.setLong(DataStore.ORDER_CARD, timeInMillis)
    }

    override fun getOrderCardTime(): Flow<Long> = storeService.getLong(DataStore.ORDER_CARD, 0L)

    override suspend fun setStoryTime(storyTime: Long) {
        storeService.setLong(DataStore.STORY_TIME, storyTime)
    }

    override fun getStoryTime(): Flow<Long> = storeService.getLong(DataStore.STORY_TIME, 0L)


    override suspend fun logOut() {
        setTokenType(null)
        setRefreshToken(null)
        setAccessToken(null)
        setPhoneCode(null)
        setPhoneNumber(null)
        setEmail(null)
        setSwitchState(SwitchState.BIKE)
        setFirebaseToken(null)
        setOrderCardTime(0L)
        setStoryTime(0L)
    }
}