package com.android.mimo.domain.model

enum class SwitchState {

    BIKE,
    SCOOTER,
    CHARGER
}