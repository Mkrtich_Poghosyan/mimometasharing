package com.android.mimo.core

data class BaseResult<T>(
    val timestamp: String,
    val statusCode: Int,
    val status: String,
    val message: String,
    var content: T?
)