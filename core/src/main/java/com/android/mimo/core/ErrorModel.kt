package com.android.mimo.core

data class ErrorModel(
    val timestamp: String,
    val statusCode: Int,
    val status: String,
    val message: String
)
