package com.android.mimo.data.di

import android.app.Application
import androidx.room.Room
import com.android.mimo.data.dataservice.apiservice.AccountApiService
import com.android.mimo.data.dataservice.room.AppDatabase
import com.android.mimo.data.dataservice.room.dao.StringResourcesDao
import com.android.mimo.data.interceptor.HeaderInterceptor
import com.android.mimo.domain.datastore.DataStore
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.core.annotation.ComponentScan
import org.koin.core.annotation.Module
import org.koin.core.annotation.Single
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

@Module
@ComponentScan("com.android.mimo.data")
class DataModule {

    @Single
    fun providesInterceptor(dataStore: DataStore): HeaderInterceptor =
        HeaderInterceptor(dataStore)

    @Single
    fun providesLoggingInterceptor(): HttpLoggingInterceptor {
        return HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
    }

    private fun providesOkHttpClient(
        httpLoggingInterceptor: HttpLoggingInterceptor,
        headerInterceptor: HeaderInterceptor,
        timeOut: Long = 30
    ): OkHttpClient = OkHttpClient
        .Builder()
        .addInterceptor(headerInterceptor)
        .addInterceptor(httpLoggingInterceptor)
        .callTimeout(timeOut, TimeUnit.SECONDS)
        .connectTimeout(timeOut, TimeUnit.SECONDS)
        .readTimeout(timeOut, TimeUnit.SECONDS)
        .writeTimeout(timeOut, TimeUnit.SECONDS)
        .build()

    private fun provideRetrofit(okHttpClient: OkHttpClient, baseUrl: String): Retrofit =
        Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(baseUrl)
            .client(okHttpClient)
            .build()

    @Single
    fun provideLocationService(
        headerInterceptor: HeaderInterceptor,
        httpLoggingInterceptor: HttpLoggingInterceptor
    ): AccountApiService {
        val client = providesOkHttpClient(httpLoggingInterceptor, headerInterceptor)
        val retrofit = provideRetrofit(client, "b")
        return retrofit.create(AccountApiService::class.java)
    }


    @Single
    fun provideGenreDao(db: AppDatabase): StringResourcesDao = db.resourcesDao()

    @Single
    fun provideDatabase(application: Application): AppDatabase {
        return Room.databaseBuilder(application, AppDatabase::class.java, "RadioDB")
            //.fallbackToDestructiveMigration()
            .allowMainThreadQueries()
            .build()
    }

}