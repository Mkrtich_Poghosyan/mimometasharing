package com.android.mimo.data.interceptor

import com.android.mimo.domain.datastore.DataStore
import com.android.mimo.entities.BaseResponse
import com.google.gson.reflect.TypeToken
import com.squareup.moshi.Moshi
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import okhttp3.Interceptor
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.Request
import okhttp3.RequestBody.Companion.toRequestBody
import okhttp3.Response
import org.json.JSONObject
import org.koin.core.annotation.Singleton
import java.lang.reflect.Type

@Singleton
class HeaderInterceptor(
    private val dataStore : DataStore
) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {

        return runBlocking {
            getNewToken(chain)
        }
    }

    private suspend fun getNewToken(chain: Interceptor.Chain) : Response {

        val originalRequest = chain.request()
        val requestBuilder = originalRequest.newBuilder()
            .method(originalRequest.method, originalRequest.body)
            .header("Content-Type", "application/json")
            .header("Accept-Language", getLocal())
            .header("Authorization", getAccessTokenAndType())

        val newRequest: Request = requestBuilder.build()
        val initialResponse = chain.proceed(newRequest)

        if (initialResponse.code == 401) {
            val mediaType = "application/json;v=1.0".toMediaTypeOrNull()
            val jsonObject = JSONObject()
            jsonObject.put("token", dataStore.getRefreshToken().first())
            jsonObject.put("deviceId", dataStore.getDeviceID().first())
            val body = jsonObject.toString().toRequestBody(mediaType)
            val refreshTokenRequest =
                originalRequest
                    .newBuilder()
                    .post(body)
                    .url(AUTH_API_URL+"auth/api/auth/refresh-token")
                    .build()
            initialResponse.close()
            val refreshResponse = chain.proceed(refreshTokenRequest)
            if (refreshResponse.code == 200 || refreshResponse.code == 201) {
                val type: Type = object : TypeToken<BaseResponse<SignInResponseModel?>?>() {}.type

                val jsonAdapter = Moshi.Builder().build().adapter<BaseResponse<SignInResponseModel?>?>(type)
                refreshResponse.body?.let {
                    val response : BaseResponse<SignInResponseModel?>? = jsonAdapter.fromJson(it.source())
                    response?.value?.let {value ->
                        dataStore.setAccessToken(value.accessToken)
                        dataStore.setRefreshToken(value.refreshToken)
                        dataStore.setTokenType(value.tokenType)
                    }
                }

                val newCall = originalRequest.newBuilder()
                    .header("Content-Type", "application/json")
                    .header("Accept-Language", getLocal())
                    .header("Authorization", getAccessTokenAndType())
                    .build()
                refreshResponse.close()
                return chain.proceed(newCall)
            } else {
                return refreshResponse
            }
        }

        return initialResponse
    }

    private suspend fun getAccessTokenAndType() : String{
        return "${dataStore.getTokenType().first()?:""} ${dataStore.getAccessToken().first()?:""}"
    }

    private suspend fun getLocal() : String{
        return dataStore.getLanguage().first()?:"en"
    }

}