package com.android.mimo.data.dataservice.apiservice


import com.android.mimo.core.BaseResult
import com.android.mimo.entities.user.response.UserResponse
import retrofit2.http.*

interface AccountApiService {

    @GET("api/user")
    suspend fun getUser(): BaseResult<UserResponse>


}