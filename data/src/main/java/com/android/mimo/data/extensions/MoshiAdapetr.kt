package com.android.mimo.data.extensions

import com.android.mimo.core.ErrorModel
import com.google.gson.reflect.TypeToken
import com.squareup.moshi.Moshi
import okhttp3.ResponseBody
import java.lang.reflect.Type

fun ResponseBody.convertErrorModel(): ErrorModel? {
    return try {
        val type: Type = object : TypeToken<ErrorModel?>() {}.type
        val jsonAdapter = Moshi.Builder().build().adapter<ErrorModel?>(type)
        jsonAdapter.fromJson(string())
    }catch(ex : java.lang.Exception) {
        null
    }
}
