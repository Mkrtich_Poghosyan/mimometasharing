package com.android.mimo.data.extensions


import android.content.res.Resources.NotFoundException
import com.android.mimo.core.ActionResult
import com.android.mimo.core.CallException
import com.android.mimo.entities.BaseResponse
import retrofit2.HttpException
import retrofit2.Response
import java.net.UnknownHostException

const val NO_INTERNET_CONNECTION_ERROR_CODE = -1001
const val HTTP_EXCEPTION_ERROR_CODE = -1002
const val DEFAULT_ERROR_CODE = -1003
const val HTTP_NOT_FOUND = 404
const val INTERNAL_SERVER_ERROR = 500
const val AUTHENTICATION_ERROR_CODE = 401
const val FORBIDDEN_ERROR_CODE = 403

suspend fun  <R> makeApiCall(call: suspend () -> ActionResult<R>) = try {
    call()
} catch (exception: UnknownHostException) {
    ActionResult.Error(CallException(NO_INTERNET_CONNECTION_ERROR_CODE, "Please Check Internet Connection", null))
} catch (exception: HttpException) {
    ActionResult.Error(CallException(HTTP_EXCEPTION_ERROR_CODE, exception.message, null))
}catch (e : Exception){
    e.printStackTrace()
    ActionResult.Error(CallException(DEFAULT_ERROR_CODE, "Something went wrong", null))
}catch (e : NotFoundException){
    ActionResult.Error(CallException(404, "Something went wrong", null))
}

fun <T> analyzeResponse(
    response: Response<BaseResponse<T>>
): ActionResult<BaseResponse<T>> {
    val responseBody = response.body()

    when {
        response.isSuccessful -> {
            responseBody?.let {
                return  ActionResult.Success(it)
            } ?: return ActionResult.Error(CallException(response.code(), response.message(), response.errorBody()?.convertErrorModel()))
        }
        else -> {
            return ActionResult.Error(CallException(response.code(), response.message(),response.errorBody()?.convertErrorModel()))
        }
    }
}



