package com.android.mimo.data.dataservice.room.dao

import androidx.room.Dao
import androidx.room.Query
import com.android.mimo.data.model.room.StringResourcesModel

@Dao
interface StringResourcesDao {

    @Query("SELECT * FROM ${StringResourcesModel.TABLE_NAME} where `${StringResourcesModel.COLUMN_KEY}`=:primaryKey")
    fun findStringByKey(primaryKey: String): StringResourcesModel?

}