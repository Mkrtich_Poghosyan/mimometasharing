package com.android.mimo.data.model.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = StringResourcesModel.TABLE_NAME)
data class StringResourcesModel(

    @PrimaryKey
    @ColumnInfo(name = COLUMN_KEY)
    val key: String,
    @ColumnInfo(name = COLUMN_VALUE)
    val value: String?
){
    companion object{
        const val TABLE_NAME = "string_resources_model"
        const val COLUMN_KEY = "key"
        const val COLUMN_VALUE = "value"
    }
}
