package com.android.mimo.data.dataservice.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.android.mimo.data.dataservice.room.dao.StringResourcesDao
import com.android.mimo.data.model.room.StringResourcesModel

@Database(
    entities = [StringResourcesModel::class],
    version = 1,
    exportSchema = false
)

abstract class AppDatabase : RoomDatabase() {
    abstract fun resourcesDao(): StringResourcesDao
}