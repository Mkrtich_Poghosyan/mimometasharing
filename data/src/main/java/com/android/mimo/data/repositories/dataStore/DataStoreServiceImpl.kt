package com.android.mimo.data.repositories.dataStore

import android.content.*
import androidx.datastore.core.*
import androidx.datastore.preferences.*
import androidx.datastore.preferences.core.*
import com.android.mimo.domain.repositories.datastore.DataStoreService
import kotlinx.coroutines.flow.*

@Suppress("UNCHECKED_CAST")
class DataStoreServiceImpl (context: Context) : DataStoreService {

	private val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = "info")
	private val result = context.dataStore

	private suspend fun set(key: String, value: Any?) {
		when (value) {
			is Int -> result.edit { edit ->
				val tokenKey = preferencesKey<Int>(key)
				edit[tokenKey] = value
			}
			is Long -> result.edit { edit ->
				val tokenKey = preferencesKey<Long>(key)
				edit[tokenKey] = value
			}
			is Float -> result.edit { edit ->
				val tokenKey = preferencesKey<Float>(key)
				edit[tokenKey] = value
			}
			is String -> result.edit { edit ->
				val tokenKey = preferencesKey<String>(key)
				edit[tokenKey] = value
			}
			is Boolean -> result.edit { edit ->
				val tokenKey = preferencesKey<Boolean>(key)
				edit[tokenKey] = value
			}
			else -> throw UnsupportedOperationException("Not implemented type")
		}
	}

	private inline operator fun <reified T> get(
		key: String, t : T
	): Flow<T> {
		val preferencesKey = intPreferencesKey(key)
		return when (T::class) {
			Int::class ->
				result.data.map { preferences ->
					preferences[preferencesKey<Int>(key)] ?: t
				} as Flow<T>
			Long::class ->
				result.data.map { preferences ->
					preferences[preferencesKey<Long>(key)] ?: t
				} as Flow<T>
			Float::class ->
				result.data.map { preferences ->
					preferences[preferencesKey<Float>(key)] ?: t
				} as Flow<T>

			Double::class ->
				result.data.map { preferences ->
					preferences[preferencesKey<Double>(key)] ?: t
				} as Flow<T>
			String::class ->
				result.data.map { preferences ->
					preferences[preferencesKey<String>(key)] ?: t
				} as Flow<T>
			Boolean::class ->
				result.data.map { preferences ->
					preferences[preferencesKey<Boolean>(key)] ?: t
				} as Flow<T>
			else -> throw UnsupportedOperationException("Not implemented type")
		}
	}

	private inline fun <reified T> preferencesKey(key: String): Preferences.Key<T> {
		return when (T::class) {
			Int::class -> intPreferencesKey(key) as Preferences.Key<T>
			Long::class -> longPreferencesKey(key) as Preferences.Key<T>
			Float::class -> floatPreferencesKey(key) as Preferences.Key<T>
			Double::class -> floatPreferencesKey(key) as Preferences.Key<T>
			String::class -> stringPreferencesKey(key) as Preferences.Key<T>
			Boolean::class -> booleanPreferencesKey(key) as Preferences.Key<T>
			else -> throw UnsupportedOperationException("Preferences key type is not found")
		}
	}


	override suspend fun setString(key: String, value: String?) {
		set(key,value)
	}

	override fun getString(key: String): Flow<String?> = get(key, null)

	override suspend fun setInt(key: String, value: Int) {
		set(key,value)
	}

	override fun getInt(key: String, defaulterValue: Int): Flow<Int> = get(key, defaulterValue)

	override suspend fun setBoolean(key: String, value: Boolean) {
		set(key,value)
	}

	override fun getBoolean(key: String, defaulterValue: Boolean): Flow<Boolean> = get(key, defaulterValue)

	override suspend fun setFloat(key: String, value: Float) {
		set(key,value)
	}

	override fun getFloat(key: String, defaulterValue: Float): Flow<Float> = get(key, defaulterValue)

	override suspend fun setDouble(key: String, value: Double) {
		set(key,value)
	}

	override fun getDouble(key: String, defaulterValue: Double): Flow<Double> = get(key, defaulterValue)

	override suspend fun setLong(key: String, value: Long) {
		set(key,value)
	}

	override fun getLong(key: String, defaulterValue: Long): Flow<Long> = get(key, defaulterValue)
}